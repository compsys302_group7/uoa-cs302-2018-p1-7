# UoA-CS302-2018-P1-7

COMPSYS302 Group 7

Dave Langdon
UPI: dlan445

Ikgue(Kevin) Lee
UPI: ilee471

Compilation instructions:

1. Download the files for the game and extract to a local directory on your computer.
2. Open Eclipse, Import button (under Files tab).
3. Under the Git folder, select “Projects from Git”, then “Existing local repository”. 
4. Select “add” then navigate to the folder that you have extracted the game to. A folder called "uoa-cs302-2018-p1-7" should be there.
5. Import the project.
6. You should see the whole project directory on the left side, including the “macpac_by_kave” folder.
7. Right click and select "Run As" -> "Java Application". It may take some time to build the program. After that the program should run.