package uoa.compsys302.address;


import javafx.scene.paint.Color;

import javafx.scene.shape.Circle;

public class Maze extends GameData{
	//Setting the block size for each square
	protected static final int BLOCK_SIZE = 50;
	
	Maze(int Level) {
		
	}
	
	//
	// 		7 			6 		 	5 		4 		3 			2 		1
	// 		ammo pwr	eat pwr		pellet	ln btm	ln right	ln up	ln left
	//		How the maze array data is structured - all numbers in powers of 2 (binary)
	
	public static void setMaze() {
		int x,y;
		short i=0;
		
		//Data arrays for maps
		final short screenData[] = {
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0 ,0 ,35,26,26,26,18,26,26,26,26,26,70,2 ,6 ,18,26,
				0 ,0 ,21,0 ,0 ,4 ,20,8 ,8 ,8 ,8 ,12,20,0 ,4 ,20,0 , 
				0 ,0 ,21,0 ,0 ,4 ,16,24,24,24,24,24,20,0 ,4 ,20,0 ,
				0 ,0 ,21,8 ,8 ,12,20,8 ,8 ,8 ,8 ,12,20,0 ,4 ,20,0 ,
				0 ,0 ,17,24,16,24,16,24,24,24,24,24,20,8 ,12,20,0 ,
				0 ,0 ,21,12,20,4 ,20,8 ,8 ,8 ,8 ,12,16,24,24,20,0 ,
				0 ,0 ,17,24,28,4 ,24,24,16,24,24,24,20,0 ,4 ,20,0 ,
				0 ,0 ,21,8 ,8 ,8 ,8 ,12,20,8 ,8 ,12,20,8 ,12,20,0 ,
				0 ,0 ,17,24,16,24,24,24,24,24,16,24,24,24,24,20,0 ,
				0 ,0 ,21,4 ,20,8 ,8 ,8 ,8 ,12,20,8 ,8 ,8 ,12,20,0 ,
				0 ,0 ,21,4 ,16,24,16,24,24,24,16,24,24,24,16,44,0 ,
				0 ,0 ,21,4 ,20,12,20,8 ,0 ,4 ,20,8 ,8 ,12,20, 4,0 ,
				0 ,0 ,21,12,16,16,24,20,8 ,12,16,24,24,24,28, 4,0 ,
				10,26,24,24,24,28,12,24,24,24,76,8 ,8 ,8 ,8 ,12,0
		};
		final short level2[] = {
			19, 18, 18,  9,  8,  0,  0,  0,  0,  0,  0,  0,  8, 12, 18, 18, 22,
			17,  7, 16, 16, 16,  9,  8,  8,  8,  8,  8, 12, 16, 16, 16,  7, 20,
			17,  9, 10,  6, 16, 16, 16, 16, 16, 16, 16, 16, 16,  3, 10, 12, 20,
			17, 16, 16,  5, 16, 15, 16,  7, 16,  7, 16, 15, 16,  5, 16, 16, 20,
			17,  7, 16,  5, 16, 16, 16,  5, 16,  5, 16, 16, 16,  5, 16,  7, 20,
			17, 13, 16,  9, 10,  2, 10, 12, 16,  9, 10,  2, 10, 12, 16, 13, 20,
			17, 16, 16, 16, 16, 13, 16, 16, 16, 16, 16, 13, 16, 16, 16, 16, 20,
			17, 11, 10,  6, 16, 16, 16,  7,128,  7, 16, 16, 16,  3, 10, 14, 20,
			17, 16, 16,  9, 10,  6, 16,  9, 10, 12, 16,  3, 10, 12, 16, 16, 20,
			17,  7, 16, 16, 16,  5, 16, 16, 16, 16, 16,  5, 16, 16, 16,  7, 20,
			17,  9,  2,  6, 16,  9, 10, 10,  2, 10, 10, 12, 16,  3,  2, 12, 20,
			17, 16,  9,  4, 16, 16, 16, 16,  5, 16, 16, 16, 16,  1, 12, 16, 20,
			 6, 16, 16,  9, 10, 10, 14, 16, 13, 16, 11, 10, 10, 12, 16, 16,  3,
			 0,  6, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,  3,  0
		};
		
		//For drawing the map - looping through every value in the array
		for (y=0 ; y<BLOCK_SIZE*15 ; y+=BLOCK_SIZE) {
			for (x=0 ; x<BLOCK_SIZE*17 ; x+=BLOCK_SIZE) {
				
				// Draws left line
				if ((screenData[i] & 1) != 0) { 
					wallCreation(x, y, x, y + BLOCK_SIZE - 1, true);
                    //Line newline = new Line (x, y, x, y + BLOCK_SIZE - 1);
                    //entityGroup.getChildren().add(newline);
                }
				
				// Draws top line
                if ((screenData[i] & 2) != 0) { 
                	wallCreation(x, y, x + BLOCK_SIZE - 1, y, false);
                    //Line newline2 = new Line(x, y, x + BLOCK_SIZE - 1, y);
                    //entityGroup.getChildren().add(newline2);
                }
                
                // Draws right line
                if ((screenData[i] & 4) != 0) { 
                	wallCreation(x + BLOCK_SIZE - 1, y, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1, true);
                    //Line newline3 = new Line(x + BLOCK_SIZE - 1, y, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                    //entityGroup.getChildren().add(newline3);
                }
                
                // Draws bottom line
                if ((screenData[i] & 8) != 0) { 
                	wallCreation(x, y + BLOCK_SIZE - 1, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1, false);
                    //Line newline4 = new Line(x, y + BLOCK_SIZE - 1, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                    //entityGroup.getChildren().add(newline4);
                }
                
                // Draws pellet
                if ((screenData[i] & 16) != 0) {
                	//Rectangle rectangle = new Rectangle(x+BLOCK_SIZE/2,y+BLOCK_SIZE/2,2,2);
                    //entityGroup.getChildren().add(rectangle);
                    
            		pelletControl(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);				
                }
                
                // Draws eat powerup
                if ((screenData[i] & 32) != 0) {
                	Circle circle = new Circle(x+BLOCK_SIZE/2,y+BLOCK_SIZE/2,BLOCK_SIZE/6,Color.RED);
                	entityGroup.getChildren().add(circle);
                }
                
                // Draws ammo powerup
                if ((screenData[i] & 64) != 0) {
                	Circle circle = new Circle(x+BLOCK_SIZE/2,y+BLOCK_SIZE/2,BLOCK_SIZE/6,Color.CHOCOLATE);
                	entityGroup.getChildren().add(circle);
                }
                i++;
			}
		}
	}

}
