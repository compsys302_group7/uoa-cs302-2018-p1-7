package uoa.compsys302.address;

public class Wall extends Entity {
	
	int xLoc;
	int yLoc;
	int xLoc2;
	int yLoc2;
	
	Wall() {
		bound = new BoundingBox(xLoc, yLoc, (xLoc + 100), (yLoc + 100), TYPE_WALL);
	}
	
	public int getX() {
		return this.xLoc;
	}
	
	public int getY() {
		return this.yLoc;
	}
	
	public int getX2() {
		return this.xLoc2;
	}
	
	public int getY2() {
		return this.yLoc2;
	}
	
	void setX(int value) {
		xLoc = value;
	}
	
	void setY(int value) {
		yLoc = value;
	}

}
