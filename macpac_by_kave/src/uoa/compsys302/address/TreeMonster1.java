package uoa.compsys302.address;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class TreeMonster1 extends EnemyMoving {
	
	TreeMonster1(int x, int y) {
		this.xLoc = x;
		this.yLoc = y;
		bound.setBounds(getX(), getY(), (getX() + ENTITY_DIMENSIONS) , (getY() + ENTITY_DIMENSIONS));
	}
	
	public ImageView draw() {
		Image image = new Image(getClass().getResourceAsStream("Resources/Tree_Monster1_1.png"));
		ImageView imageview = new ImageView(image);
		imageview.setX(this.getX());
		imageview.setY(this.getY());
		imageview.setFitHeight(3*ENTITY_DIMENSIONS);
		imageview.setFitWidth(3*ENTITY_DIMENSIONS);
		return imageview;
	}

}
