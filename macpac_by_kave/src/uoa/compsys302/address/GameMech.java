package uoa.compsys302.address;

public class GameMech extends GameData {

	public int currencyHeld;
	public int level;
	public int localScore;
	public int heartId;
	protected static final int CHAR_HEALTH_DEFAULT = 3;
	
	GameMech(int currencyHeld, int level, int localScore) {
		this.currencyHeld = currencyHeld;
		this.level = level;
		this.localScore = localScore;
		
		initHUD();
	}
	
	void initHUD() {
		for (int i = 0; i < CHAR_HEALTH_DEFAULT; i++) {		// creating display hearts
			HealthDisplay heart = new HealthDisplay();
			entityGroup.getChildren().add(heart.draw());
			//heartId = entityGroup.getChildren().li
			//System.out.print("Heart id " + heartId);
		}
	}
	
	void updateHealth(HealthDisplay heart) {
		for (int i = 0; i < CHAR_HEALTH_DEFAULT; i++) {
			//heart.remove();
		}
	}
	
	
	
}
