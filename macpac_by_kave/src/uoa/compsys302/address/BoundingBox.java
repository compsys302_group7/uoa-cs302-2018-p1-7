package uoa.compsys302.address;

public class BoundingBox {
	
	private int xStart;
	private int xFinish;
	private int yStart;
	private int yFinish;
	private int type;	// 0: player; 1: enemy; 2: pellet
	
	BoundingBox(int xStart, int xFinish, int yStart, int yFinish, int type) {
		this.xStart = xStart;
		this.yStart = yStart;
		this.xFinish = xFinish;
		this.yFinish = yFinish;
		this.type = type;
	}
	
	public int getxStart() {
		return this.xStart;
	}
	
	public int getxFinish() {
		return this.xFinish;
	}
	
	public int getyStart() {
		return this.yStart;
	}
	
	public int getyFinish() {
		return this.yFinish;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setBounds(int x, int y, int x2, int y2) {
		this.xStart = x;
		this.yStart = y;
		this.xFinish = x2;
		this.yFinish = y2;
		
	}
	
	public BoundingBox getBoundingBox() {
		return this;
	}

}
