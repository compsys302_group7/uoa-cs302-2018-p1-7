package uoa.compsys302.address;

import javafx.scene.shape.Line;

public class WallH extends Wall {
	
	// vertical wall (top, bottom walls)
	WallH(int x, int y, int x2, int y2) {
		this.xLoc = x;
		this.yLoc = y;
		this.xLoc2 = x2;
		this.yLoc2 = y2;
		bound.setBounds(getX(), getY(), (getX() + Maze.BLOCK_SIZE) , (getY() + 50));
	}
	
	public Line draw(int x, int y, int x2, int y2) {
		Line wall = new Line (this.getX(), this.getY(), this.getX2(), this.getY2());
		return wall;
	}

}
