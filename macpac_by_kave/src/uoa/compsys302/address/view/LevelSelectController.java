package uoa.compsys302.address.view;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import uoa.compsys302.Main;

public class LevelSelectController {
	// Declaring a string "Level" for use in displaying different fxml screens for different level selected
	public static String Level;
	
	@FXML
	public Button oneButton;
	
	@FXML
	public Button twoButton;
	
	@FXML
	public Button threeButton;
	
	@FXML
	public Button fourButton;
	
	// For button for returning to main menu (starting screen)
	@FXML
	private void returnToMain(ActionEvent event) throws IOException{
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/StartingScreen.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	// For selecting level 1
	@FXML
	private void LevelSelect(ActionEvent event) throws IOException{
		Level = oneButton.getText();
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/CharacterSelect.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	// For selecting level 2
	@FXML
	private void LevelSelect2(ActionEvent event) throws IOException{
		Level = twoButton.getText();
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/CharacterSelect.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	// For selecting level 3
	@FXML
	private void LevelSelect3(ActionEvent event) throws IOException{
		Level = threeButton.getText();
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/CharacterSelect.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	// For selecting level 4
	@FXML
	private void LevelSelect4(ActionEvent event) throws IOException{
		Level = fourButton.getText();
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/CharacterSelect.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
