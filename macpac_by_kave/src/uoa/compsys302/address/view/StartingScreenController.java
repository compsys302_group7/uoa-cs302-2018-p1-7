package uoa.compsys302.address.view;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import uoa.compsys302.Main;

public class StartingScreenController {
	public Stage primaryStage;
	
	public void setPrimaryStage(Stage stage) {
		this.primaryStage = stage;
	}
	
	// Brings up Level Select screen when Story Mode button is pressed
	@FXML
	private void storyModeButton(ActionEvent event) throws IOException {
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/LevelSelect.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	// Brings up help screen when help button pressed
	@FXML
	private void helpButton(ActionEvent event) throws IOException {
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/Help.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}