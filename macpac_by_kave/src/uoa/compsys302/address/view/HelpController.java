package uoa.compsys302.address.view;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import uoa.compsys302.Main;

public class HelpController {
	//Button for going back to the main menu (starting screen)
	@FXML
	private void MainMenu(ActionEvent event) throws IOException {
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/StartingScreen.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
