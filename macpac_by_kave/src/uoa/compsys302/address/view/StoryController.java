package uoa.compsys302.address.view;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;
import uoa.compsys302.address.GameData;

public class StoryController {
	
	// For "confirm" button on story - Sets the maze and starts the game
	@FXML
	private void startTheGame(ActionEvent event) throws IOException{
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		GameData.startGame(stage);
	}
}
