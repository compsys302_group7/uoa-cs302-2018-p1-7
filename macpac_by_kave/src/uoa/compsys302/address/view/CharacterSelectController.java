package uoa.compsys302.address.view;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import uoa.compsys302.Main;
import uoa.compsys302.address.GameData;

public class CharacterSelectController {
	public Stage primaryStage;
	
	//For Level Select button - returns to Level Select screen
	@FXML
	private void returnToLevel(ActionEvent event) throws IOException{
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/LevelSelect.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	//Sets the maze and starts the game with character
	@FXML
	private void startTheGame(ActionEvent event) throws IOException{
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		GameData.startGame(stage);
	}
	
	//Shows story screen with a bit of writing depending on the level selected
	@FXML
	private void startStory(ActionEvent event) throws IOException{
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		String url = "address/view/Story"+LevelSelectController.Level+".fxml";
		Pane root = FXMLLoader.load(Main.class.getResource(url));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
