package uoa.compsys302.address;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class HealthDisplay extends HUDObjects {

	static int incrementPos = 0;
	
	HealthDisplay() {
		this.xLoc = 20 + incrementPos;
		this.yLoc = 20;
		incrementPos = incrementPos + 24;
	}
	
	public Circle draw() {
		Circle health = new Circle(this.getX(), this.getY(), 10, Color.PALEVIOLETRED);
		return health;
	}
}
