package uoa.compsys302.address;

import javafx.scene.shape.Line;

public class WallV extends Wall {

	// vertical wall (left, right walls)
	WallV(int x, int y, int x2, int y2) {
		this.xLoc = x;
		this.yLoc = y;
		this.xLoc2 = x2;
		this.yLoc2 = y2;
		bound.setBounds(getX(), getY(), (getX() + 50) , (getY() + Maze.BLOCK_SIZE));
	}
	
	public Line draw(int x, int y, int x2, int y2) {
		
		Line wall = new Line (this.getX(), this.getY(), this.getX2(), this.getY2());
		
		return wall;
	}
	
}
