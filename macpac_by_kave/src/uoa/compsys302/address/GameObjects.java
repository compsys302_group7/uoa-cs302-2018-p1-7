package uoa.compsys302.address;

public class GameObjects extends Entity {
	
	static int incrementPos = 0;	// only used for generating grid of pellets (testing purposes)
	int xLoc;
	int yLoc;
	boolean active;
	//BoundingBox bound = new BoundingBox(xLoc, yLoc, (xLoc + PELLET_DIMENSIONS), (yLoc + PELLET_DIMENSIONS));
	
	GameObjects() {
		bound = new BoundingBox(xLoc, yLoc, (xLoc + PELLET_DIMENSIONS), (yLoc + PELLET_DIMENSIONS), TYPE_PELLET);
	}
	
	public int getX() {
		return this.xLoc;
	}
	
	public int getY() {
		return this.yLoc;
	}
	
	public boolean returnActive() {
		return this.active;
	}
	
	void setX(int value) {
		xLoc = value;
	}
	
	void setY(int value) {
		yLoc = value;
	}
	

}
