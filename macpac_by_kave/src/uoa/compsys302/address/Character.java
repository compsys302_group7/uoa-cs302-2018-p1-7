	package uoa.compsys302.address;

import java.io.IOException;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import uoa.compsys302.Main;

public class Character extends Entity {
	
	int p1X;	// stored location when movement. UGLY ASF (sorry!)
	int p2X;
	int p1Y;
	int p2Y;
	int health;
	
	Character() {
		bound = new BoundingBox(xLoc, yLoc, (xLoc + ENTITY_DIMENSIONS), (yLoc + ENTITY_DIMENSIONS), TYPE_PLAYER);
		this.health = GameMech.CHAR_HEALTH_DEFAULT;
	}
	
	public int getHealth() {
		return this.health;
	}
	
	public void setHealth(int value) {
		this.health = value;
	}
	
	public void move(Scene scene, Character p1, Character p2, boolean gamePaused, Stage stage) {
			
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override public void handle(KeyEvent event) {
				
				// save current pos in case movement causes collision
				p1X = p1.getX();
				p1Y = p1.getY();
				p2X = p2.getX();
				p2Y = p2.getY();
				
				if (gamePaused) {
					System.out.println("game paused");
					return;
				}
 				
				switch (event.getCode()) {
				
					// P1 movement
					case W: 	p1.setY(p1.getY() - ENTITY_MOVEMENT_STEP);
								updateGroup(p1, p2);
								break;
					case A:		p1.setX(p1.getX() - ENTITY_MOVEMENT_STEP); 
								updateGroup(p1, p2);
								break;
					case S:		p1.setY(p1.getY() + ENTITY_MOVEMENT_STEP); 
								updateGroup(p1, p2);
								break;
					case D:		p1.setX(p1.getX() + ENTITY_MOVEMENT_STEP); 
								updateGroup(p1, p2);
								break;
							
					// P2 movement
					case UP: 	p2.setY(p2.getY() - ENTITY_MOVEMENT_STEP);
								updateGroup(p1, p2);
								break;				
					case LEFT:	p2.setX(p2.getX() - ENTITY_MOVEMENT_STEP); 
								updateGroup(p1, p2);
								break;
					case DOWN:	p2.setY(p2.getY() + ENTITY_MOVEMENT_STEP); 
								updateGroup(p1, p2);
								break;
					case RIGHT:	p2.setX(p2.getX() + ENTITY_MOVEMENT_STEP); 
								updateGroup(p1, p2);
								break;
								
					// gameplay	
					// to get keybinds: go to javafx.scene.input.KeyCode(.class)
							
					// game continues
					case PAGE_UP: 	System.out.println("PageUp pressed. Game continue.");
									break;
					
					// timer = 0;
					case PAGE_DOWN:	
									try {
										GameData.gameOver(p1);
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									//System.out.println("(Stopped via keypress)");
									break;
									
					// pause
					case P:			//System.out.println("Toggle pause.");
									break;
									
					// quit
					case ESCAPE:	try {
										storyModeButton(stage);
									} catch (IOException e) {
										e.printStackTrace();
									}
									break;
								
					// corner cases
					default:		return;
				}
			}
		});
	}
	
	private void storyModeButton(Stage stage) throws IOException {
		Pane root = FXMLLoader.load(Main.class.getResource("address/view/StartingScreen.fxml"));
		final Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public void updateGroup(Character p1, Character p2) {
		
		p1.bound.setBounds(p1.getX() - ENTITY_DIMENSIONS, p1.getY() - ENTITY_DIMENSIONS, (p1.getX() + ENTITY_DIMENSIONS) , (p1.getY() + ENTITY_DIMENSIONS));
		p2.bound.setBounds(p2.getX() - ENTITY_DIMENSIONS, p2.getY() - ENTITY_DIMENSIONS, (p2.getX() + ENTITY_DIMENSIONS) , (p2.getY() + ENTITY_DIMENSIONS));
		
		// if collision occurs (i.e. bad move) (use for walls too)
		if ((p1.updateBounds(p1.getID(), p1.bound) == 0) || (p2.updateBounds(p2.getID(), p2.bound) == 0)) {
			resetPos(p1, p1X, p1Y);
			resetPos(p2, p2X, p2Y);
		}
		// if touch enemy
		if (p1.updateBounds(p1.getID(), p1.bound) == 1) {
			lifeLost(p1);
			//GameMech.updateHealth(Gameplay.entityGroup.getChildren().remove(heart));
			//Gameplay.entityGroup.getChildren().remove(heart);
			resetPos(p1, GameData.SPAWN_X, GameData.SPAWN_Y);
		}
		// if touch pellet
		if (p1.updateBounds(p1.getID(), p1.bound) == 2) {
			GameData.pelletControl(p1.getX(), p1.getY());
		}
		// if touch wall test
		if (p1.updateBounds(p1.getID(), p1.bound) == 5) {
			resetPos(p1, p1X, p1Y);
		}
		
		GameData.entityGroup.getChildren().remove(0);	// (this removes last-moved player) removes 'oldest' object from group
		GameData.entityGroup.getChildren().remove(0);	// (this removes other player)
		GameData.entityGroup.getChildren().add(0, ((MacPac_Orig) p1).draw());	// draws player in upd. pos., puts in front of other 'older' objects in group.
		GameData.entityGroup.getChildren().add(0, ((MacPac_Native) p2).draw());
		
		//clear temp pos.
		p1X = 0;
		p1Y = 0;
		p2X = 0;
		p2Y = 0;
	}
	
	public void lifeLost(Entity player) {
		((Character)player ).setHealth(((Character)player ).getHealth() - 1);
		System.out.print("Life lost. Life is = " + ((Character)player ).getHealth());
		if (((Character)player ).getHealth() == 0) {
			// game over.
			System.out.print("----- GAME IS OVER -----");
		}
	}
	
	public void resetPos(Entity player, int x, int y) {
		player.setX(x);
		player.setY(y);
		player.bound.setBounds(player.getX(), player.getY(), (player.getX() + ENTITY_DIMENSIONS) , (player.getY() + ENTITY_DIMENSIONS));
	}
}
