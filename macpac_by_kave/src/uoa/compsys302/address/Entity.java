package uoa.compsys302.address;

import javafx.scene.shape.Shape;

public class Entity extends Shape {
	
	int xLoc;
	int yLoc;
	protected static final int ENTITY_DIMENSIONS = 15;		// width,height of players, enemies. used for bounding box calcs.
	protected static final int ENTITY_MOVEMENT_STEP = 15;	// self explanatory. replaced by smooth movement+animation in future.
	protected static final int PELLET_DIMENSIONS = 2;
	protected static final int TYPE_PLAYER = 0;
	protected static final int TYPE_WALL = 5;
	protected static final int TYPE_ENEMY = 1;
	protected static final int TYPE_PELLET = 2;
	
	// arrays: 	[col][row]
	//			(name).length 		-> returns row length
	//			(name).length[x] 	-> returns col x length. (x is a row) *due to java 'ragged' arrays*
	// boundsList: [x] [y] [x2] [y2] [type] . creates map of bounds.
	private int id;
	private static int idCount = 0;
	public static int boundsList[][] = new int[1000][5];
	BoundingBox bound;
	
	Entity() {
		this.id = idCount++;		// ids start at 0
	}
	
	public int getID() {
		return this.id;
	}
	
	public int getX() {
		return this.xLoc;
	}
	
	public int getY() {
		return this.yLoc;
	}
	
	void setX(int value) {
		xLoc = value;
	}
	
	void setY(int value) {
		yLoc = value;
	}
	/*
	boolean canReturnObject(int x, int y) {
		if ((this.xLoc == x) && (this.yLoc == y)) {
			return true;
		} else {
			return false;
		}
	}
	
	// only called if 'canReturnObject' is true.
	Entity returnObject(int x, int y) {
		return this;
	}
	*/
	// returns -1 on good movement; 0,1,2... on collision type
	public int updateBounds(int id, BoundingBox bound) {
		boundsList[id][0] = bound.getxStart();
		boundsList[id][1] = bound.getyStart();
		boundsList[id][2] = bound.getxFinish();
		boundsList[id][3] = bound.getyFinish();
		boundsList[id][4] = bound.getType();
		
		int collisionCheckVal = collisionCheck(id);
		if (collisionCheckVal != -1) { // if collision (val is 0, 1, 2...)
			return collisionCheckVal;
		}
		return -1;
	}
	
	
	
	// returns 0,1,2 on collision type, -1 on good move
	public int collisionCheck(int id) {
		for (int i = 0; i < boundsList.length; i++) {				// check every row against row (id)
			if (i == id) { i++; }
			else {
				// bounding conditions are wrong. works but inconsistent.
				if (((boundsList[i][2] >= boundsList[id][0]) && 	// x2 (obj i) >= x  (obj id)
					(boundsList[i][3] >= boundsList[id][1])) &&		// y2 (obj i) >= y  (obj id)
					((boundsList[i][1] <= boundsList[id][3]) &&		// y  (obj i) <= y2 (obj id)
					(boundsList[i][0] <= boundsList[id][2]))) 		// x  (obj i) <= x2 (obj id)
				{
					switch (boundsList[i][4]) {
						case 0:	return 0;	// collided with player/wall
						case 1:	return 1;	// collided with enemy
						case 2:	return 2;	// collided with pellet
						case 5: return 5;	// wall test
					}
				} else {
					i++;
				}
			}
		}
		return -1; // no collision
	}

	@Override
	public com.sun.javafx.geom.Shape impl_configShape() {
		// TODO Auto-generated method stub
		return null;
	}
}
