package uoa.compsys302.address;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class MacPac_Orig extends Character {

	
	MacPac_Orig(int x, int y) {
		this.xLoc = x;
		this.yLoc = y;
		bound.setBounds(getX() - ENTITY_DIMENSIONS, getY() - ENTITY_DIMENSIONS, (getX() + ENTITY_DIMENSIONS) , (getY() + ENTITY_DIMENSIONS));
	}
	
	public ImageView draw() {
		Image image = new Image(getClass().getResourceAsStream("Resources/MacPac_Orig.png"));
		ImageView imageview = new ImageView(image);
		imageview.setX(this.getX());
		imageview.setY(this.getY());
		imageview.setFitHeight(3*ENTITY_DIMENSIONS);
		imageview.setFitWidth(3*ENTITY_DIMENSIONS);
		return imageview;
	}
	
}
