package uoa.compsys302.address;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Pellet extends GameObjects {
	
	Pellet(int x, int y) {
		this.xLoc = x;
		this.yLoc = y;
		this.active = true;
		bound.setBounds(getX(), getY(), (getX() + PELLET_DIMENSIONS) , (getY() + PELLET_DIMENSIONS));
	}
	
	public Circle draw() {
		Circle pellet = new Circle(this.getX(), this.getY(), PELLET_DIMENSIONS, Color.BLUE);
		return pellet;
	}
	
	public void checkPellet() {
		if (this.updateBounds(this.getID(), this.bound) == 2) {
			// ...
		}
	}

}
