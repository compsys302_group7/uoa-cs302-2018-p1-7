package uoa.compsys302.address;
import java.io.IOException;
import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import uoa.compsys302.Main;

public class GameData{

	private static Stage primaryStage;

	// time remainder display.
	private static Text textTimeRem = new Text();
	private static Text textTimeStart = new Text();
	private static Text textScore = new Text();
	private static int timeStart = 4;	// set to 4 (3 +1) to display the '3' counting down.
	private static int timeRem = 121;	// set to 121 (120 +1) to display the '120' counting down.

	public static double xRes = 1024;
	public static double yRes = 768;
	public static final int SPAWN_X = 500;
	public static final int SPAWN_Y = 500;
	
	protected static ArrayList<Shape> nodes;

	public static final Group entityGroup = new Group();

	public static void startGame(Stage stage) throws IOException{
		primaryStage = stage;

		boolean gamePaused;

		gamePaused = false;

		//basic framework for resizability; needs to be added to own fn (class?).
		//canvas.widthProperty().addListener(evt -> canvas.draw());
		//canvas.heightProperty().addListener(evt -> canvas.draw());
		//xRes = scene.getWidth();
		//yRes = scene.getHeight();

		final Scene scene = new Scene(entityGroup, xRes, yRes);			// define scene

		textTimeRem.setX(300);
		textTimeRem.setY(25);
		textTimeRem.setScaleX(1.5);
		textTimeRem.setScaleY(1.5);

		textTimeStart.setX(xRes / 2);
		textTimeStart.setY(yRes / 2);
		textTimeStart.setScaleX(10);
		textTimeStart.setScaleY(10);
		
		textScore.setX(600);
		textScore.setY(25);
		textScore.setScaleX(1.5);
		textScore.setScaleY(1.5);
		textScore.setText("Score: 0");

		entityGroup.getChildren().add(textTimeRem);
		entityGroup.getChildren().add(textTimeStart);
		entityGroup.getChildren().add(textScore);

		// players
		MacPac_Orig p1 = new MacPac_Orig(SPAWN_X, SPAWN_Y);				// define p1, p2.
		MacPac_Native p2 = new MacPac_Native(SPAWN_X, SPAWN_Y + 100);
		p1.draw();														// draw in p1, p2
		p2.draw();
		//nodes = new ArrayList<Shape>();
		//nodes.add(p1);
		//nodes.add(p2);
		entityGroup.getChildren().add(p1.draw());						// displaying p1, p2
		entityGroup.getChildren().add(p2.draw());		
		Maze.setMaze();

		// score timer start
		timeDecrease3s(scene, p1, p2, gamePaused, stage);

		// enemies
		TreeMonster1 e1 = new TreeMonster1(350, 500);
		e1.draw();
		entityGroup.getChildren().add(e1.draw());
		e1.updateBounds(e1.getID(), e1.bound);

		// game mechanics
		@SuppressWarnings("unused")
		GameMech gameMechanics = new GameMech(0, 1, 0);																		

		primaryStage.setScene(scene);
		primaryStage.setTitle("MACPAC");
		primaryStage.show();
	}


	public static void timeDecrease3s(Scene scene, Character p1, Character p2, boolean gamePaused, Stage stage) throws IOException {

		entityGroup.getChildren().remove(textTimeStart);
		timeStart--;
		textTimeStart.setText(Integer.toString(timeStart));
		entityGroup.getChildren().add(textTimeStart);
		if (timeStart == 0) {
			entityGroup.getChildren().remove(textTimeStart);
			p1.move(scene, p1, p2, gamePaused, stage);				// movement method. activated once 3s is complete
			timeDecrease120s(p1);
			// game 'unpaused'
			return;
		}

		Timeline time2m = new Timeline (new KeyFrame(
				Duration.millis(1000),
				actionEvent -> {
					try {
						timeDecrease3s(scene, p1, p2, gamePaused, stage);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}));

		time2m.play();

	}

	public static void timeDecrease120s(Character p1) throws IOException {

		entityGroup.getChildren().remove(textTimeRem);
		timeRem--;
		textTimeRem.setText("Time remaining: " + Integer.toString(timeRem));
		entityGroup.getChildren().add(textTimeRem);
		if (timeRem == 0) {
			gameOver(p1);
			return;
		}

		Timeline time2m = new Timeline (new KeyFrame(
				Duration.millis(1000),
				actionEvent -> {
					try {
						timeDecrease120s(p1);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}));

		time2m.play();
	}

	// game is over: 2min timer ended (or PgDn pressed)
	public static void gameOver(Character player) throws IOException{

		player.setHealth(0);
		timeRem = 0;
		textTimeRem.setText("GAME OVER");
		Alert alert = new Alert(AlertType.INFORMATION, "YOU HAVE DIED!", ButtonType.CLOSE);
		alert.showAndWait();

		if (alert.getResult() == ButtonType.CLOSE) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("address/view/StartingScreen.fxml"));
			Pane pane = loader.load();
			final Scene scene3 = new Scene(pane, 1024, 768);
			primaryStage.setResizable(false);
			primaryStage.setScene(scene3);
			primaryStage.show();
		}
	}

	public static void pelletControl(int x, int y) {
		Pellet pellet = new Pellet(x, y);
		entityGroup.getChildren().add(pellet.draw());
	}

	public static void wallCreation(int x, int y, int x2, int y2, boolean wallVert) {
		if (wallVert) {
			WallV wall = new WallV(x, y, x2, y2);
			//wall.updateBounds(wall.getID(), wall.bound);
			entityGroup.getChildren().add(wall.draw(x, y, x2, y2));
		} else {
			WallH wall = new WallH(x, y, x2, y2);
			//wall.updateBounds(wall.getID(), wall.bound);
			entityGroup.getChildren().add(wall.draw(x, y, x2, y2));
		}
	}
}
