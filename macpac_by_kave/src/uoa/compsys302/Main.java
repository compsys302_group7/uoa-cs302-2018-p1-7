package uoa.compsys302;

import java.io.IOException;


import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
public class Main extends Application {
	private static Stage primaryStage;
	
	public static Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		//Setting initial starting menu screen
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("address/view/StartingScreen.fxml"));
		Pane pane = loader.load();
		final Scene scene3 = new Scene(pane, 1024, 768);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene3);
		primaryStage.show();
	}
	
}